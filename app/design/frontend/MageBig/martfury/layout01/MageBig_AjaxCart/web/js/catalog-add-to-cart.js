/**
 * Copyright © magebig.com - All rights reserved.
 * See LICENSE.txt for license details.
 */

define([
    'jquery',
	'mage/translate',
], function ($, $t) {
    'use strict';

    return function (widget) {

        $.widget('mage.catalogAddToCart', widget, {
            /**
             * @private
             */
            _bindSubmit: function () {
                var self = this;

                if (this.element.data('catalog-addtocart-initialized')) {
                    return;
                }

                this.element.data('catalog-addtocart-initialized', 1);
                this.element.on('submit', function (e) {
                    e.preventDefault();
                    var next = 1;
                    var action = $(this).attr('action');
                    var btnView = $(this).parents('.product-item').find('.btn-quickview');

                    $(this).find('[name*="super"]').each(function (index, item) {
                        var $item = $(item);
                        if ($item.val() === '') {
                            next = 0;
                        }
                    });
                    if ((next === 0 || action.indexOf('options=cart') !== -1) && btnView.length) {
                        btnView.addClass('has-trigger');
                        btnView.trigger('click');
                        return;
                    } else {
                        self.submitForm($(this));
                    }
                });
            },

            /**
             * Handler for the form 'submit' event
             *
             * @param {jQuery} form
             */
            submitForm: function (form) {
                var addToCartButton, self = this;

                if (form.has('input[type="file"]').length && form.find('input[type="file"]').val() !== '') {
                    self.element.off('submit');
                    // disable 'Add to Cart' button
                    addToCartButton = $(form).find(this.options.addToCartButtonSelector);
                    addToCartButton.prop('disabled', true);
                    addToCartButton.addClass(this.options.addToCartButtonDisabledClass);
                    form.submit();
                } else {
                    self.ajaxSubmit(form);
                }
            },
			
			/**
			 * @param {String} form
			 */
			enableAddToCartButton: function (form) {
				
				var addToCartButtonTextAdded = this.options.addToCartButtonTextAdded || $t('Added'),
					self = this,
					messageEle = $(form).find('.addcart-message'),
					sku = $(form).data('product-sku'),
					addToCartButton = $(form).find(this.options.addToCartButtonSelector);
					
				if (messageEle) {
					var qty = parseFloat($(form).find('.qty').val());
					if ($('[data-cart-item-id="' + sku +'"]').val()) {
						qty += parseFloat($('[data-cart-item-id="' + sku +'"]').val());
					}
					messageEle.html("<span><i class='mbi mbi-check'></i> " + qty + " en el carrito</span>");
				}

				addToCartButton.find('span').text(addToCartButtonTextAdded);
				addToCartButton.attr('title', addToCartButtonTextAdded);

				setTimeout(function () {
					var addToCartButtonTextDefault = self.options.addToCartButtonTextDefault || $t('Add to Cart');

					addToCartButton.removeClass(self.options.addToCartButtonDisabledClass);
					addToCartButton.find('span').text(addToCartButtonTextDefault);
					addToCartButton.attr('title', addToCartButtonTextDefault);
				}, 1000);
			}
        });

        return $.mage.catalogAddToCart;
    }
});
