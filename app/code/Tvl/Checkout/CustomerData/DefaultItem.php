<?php
/**
 * Created by tvl.
 * Date: 4/27/2020
 * Time: 18:36
 * @author luyentrv@gmail.com
 */

namespace Tvl\Checkout\CustomerData;


use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\ObjectManager;

class DefaultItem extends \Magento\Checkout\CustomerData\DefaultItem
{
    protected function doGetItemData()
    {
        $data =  parent::doGetItemData();

        $_product = $this->item->getProduct();
        $stockRegistry = ObjectManager::getInstance()->get(StockRegistryInterface::class);
        $stock = $stockRegistry->getStockItem($_product->getId(), $_product->getStore()->getId());
        $data['product_increment'] = $stock->getQtyIncrements() ? : 1;
        $data['minimum_qty'] = $stock->getMinSaleQty();
        $data['item_subtotal'] = $this->checkoutHelper->formatPrice($this->item->getRowTotal());

        return $data;
    }
}